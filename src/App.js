import React, { Component } from 'react';
import {ScatterService} from './services/Scatter';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      rows: []
    }
  }

  componentWillMount() {
    new ScatterService()
      .getTableRows('account')
      .then(data => this.setState({
        rows: data.rows
      }));
  }

  render() {
    return (
      <div className="App">
        <ul>
          {this.state.rows.map(r => <li><pre>{JSON.stringify(r, false, 2)}</pre></li>)}
        </ul>
      </div>
    );
  }
}

export default App;
